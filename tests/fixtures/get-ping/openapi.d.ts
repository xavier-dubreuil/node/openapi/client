import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

declare namespace Paths {
    namespace GetPing {
        namespace Responses {
            export type $200 = string;
        }
    }
}

export interface OperationMethods {
  /**
   * GetPing
   */
  'GetPing'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPing.Responses.$200>
}

export interface PathsDictionary {
  ['/ping']: {
    /**
     * GetPing
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPing.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
