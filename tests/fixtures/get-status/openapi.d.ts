import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

declare namespace Components {
    namespace Schemas {
        export interface Status {
            health?: string;
        }
    }
}
declare namespace Paths {
    namespace GetStatus {
        namespace Responses {
            export type $200 = Components.Schemas.Status;
        }
    }
}

export interface OperationMethods {
  /**
   * GetStatus
   */
  'GetStatus'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetStatus.Responses.$200>
}

export interface PathsDictionary {
  ['/status']: {
    /**
     * GetStatus
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetStatus.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
