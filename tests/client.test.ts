import { expect, describe, it } from '@jest/globals';
import { Container, ClientService, ClientServices } from '../src';
import * as fixtures from './fixtures';
import mockAxios from 'jest-mock-axios';

export class PingService extends ClientService<fixtures.GetPing.Client> {
  constructor (protected readonly client: fixtures.GetPing.Client) {
    super(client);
  }
  async getPing (): Promise<string> {
    const { data } = await this.client.GetPing();
    return data;
  }
}

interface Services extends ClientServices<fixtures.GetPing.Client> {
  ping: PingService;
}

const content = 'pong';
const axiosRequest = {
  method: 'get',
  url: '/ping',
  data: undefined,
  headers: {},
  params: {}
};

describe('Client', () => {
  it('Simple', async () => {
    class TestContainer extends Container<fixtures.GetPing.Client> {
      public static async new (): Promise<TestContainer> {
        return await super.instantiate<TestContainer, fixtures.GetPing.Client>(
          TestContainer,
          await Container.readDefinition(fixtures.GetPing.definitionPath),
          { baseURL: `http://localhost` },
          null
        );
      }
    }

    const container = await TestContainer.new();
    const promise = container.client.GetPing();
    expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
    mockAxios.mockResponse({ data: content });
    const { data } = await promise;
    expect(data).toEqual(content);
  });

  it('Service', async () => {
    class TestContainer extends Container<fixtures.GetPing.Client, Services> {
      public static async new (): Promise<TestContainer> {
        return await super.instantiate<TestContainer, fixtures.GetPing.Client, Services>(
          TestContainer,
          await Container.readDefinition(fixtures.GetPing.definitionPath),
          { baseURL: `http://localhost` },
          {ping: PingService}
        );
      }
    }

    const container = await TestContainer.new();
    const promise = container.services.ping.getPing();
    expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
    mockAxios.mockResponse({ data: content });
    const data = await promise;
    expect(data).toEqual(content);
  });
});
