import { it, expect } from '@jest/globals';
import { Generator } from '../src/generator';
import * as fixtures from './fixtures';

describe('Generator', () => {
  it('Instantiate Null', async () => {
    const generator = new Generator();
    const openapi = generator.getOpenApi();
    expect(openapi.openapi).toEqual('3.0.1');
    expect(openapi.info.title).toEqual('');
    expect(openapi.info.version).toEqual('');
  });
  it('Set Info', async () => {
    const generator = new Generator();
    generator.setInfo({ title: 'test', version: '1.0.0' })
    const openapi = generator.getOpenApi();
    expect(openapi.info.title).toEqual('test');
    expect(openapi.info.version).toEqual('1.0.0');
  });
  it('Merge', async () => {
    const generator = new Generator();
    generator.merge(await Generator.openapiFromFile(fixtures.Empty.definitionPath));
    const openapi = generator.getOpenApi();
    expect(Object.values(openapi.paths)).toHaveLength(0);
    expect(Object.values(openapi.components!.schemas!)).toHaveLength(0);
  });
  it('Mapping', async () => {
    const generator = new Generator();
    const globalPaging = { name: "Paging" };
    const globalProject = { name: "Project" };
    const ListProject = { name: "IndividualProject" };
    generator.setMapping({
      "global": {
        "/paging": globalPaging,
        "/projects": globalProject,
      },
      "/list": {
        "/projects": ListProject,
      },
    });

    expect(generator.getMapping("/list", "/test")).toBeNull()
    expect(generator.getMapping("/list", "/projects")).toEqual(ListProject);
    expect(generator.getMapping("/list", "/paging")).toEqual(globalPaging);
  });
  it('toString', async () => {
    const generator = new Generator(await Generator.openapiFromFile(fixtures.Empty.definitionPath));
    expect(generator.toString()).toEqual(JSON.stringify(generator.getOpenApi()));
  });
  it('generateSchema', async () => {
    const generator = new Generator();
    const input = {
      projects: [
        { id: 23, name: 'project-23' },
        { id: 42, name: 'project-42' },
      ]
    };

    const schema = generator.generateSchema(input, "/test", ['']);
    expect(schema).toEqual({
      type: 'object',
      properties: {
        projects: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              name: { type: 'string' },
            }
          }
        }
      }
    });
    const unmapped = generator.getUnmapped();
    expect(unmapped).toContainEqual({endpoint: '/test', path: '/'});
    expect(unmapped).toContainEqual({endpoint: '/test', path: '/projects'});
  });
  it('generateSchema with Mapping', async () => {
    const generator = new Generator();
    generator.setMapping({
      "/list": {
        "/projects": { name: 'Project' },
        '/files': {
          schema: {
            type: 'object',
            allOf: [{
              type: 'object',
              properties: {
                path: { type: 'string' }
              }
            }]
          }
        },
      },
    });

    const input = {
      projects: [
        { id: 23, name: 'project-23' },
        { id: 42, name: 'project-42' },
      ],
      files: {
        'test.json': { path: '/path/to/test.json' },
      }
    };

    const schema = generator.generateSchema(input, "/list", ['']);
    expect(schema).toEqual({
      type: 'object',
      properties: {
        projects: {
          type: 'array',
          items: {
            $ref: '#/components/schemas/Project',
          }
        },
        files: {
          type: 'object',
          allOf: [{
            type: 'object',
            properties: {
              path: { type: 'string' }
            }
          }]
        }
      }
    });
    const openapi = generator.getOpenApi();
    expect(openapi.components?.schemas).toHaveProperty('Project');
    expect(openapi.components?.schemas?.Project).toEqual({
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string' },
      }
    })
});
});
