import { it, expect } from '@jest/globals';
import { merge } from '../src/merge';
import * as fixtures from './fixtures';

describe('Merge', () => {
  it('Simple', async () => {
    const openapi = await merge([fixtures.GetPing.definitionPath, fixtures.GetStatus.definitionPath]);
    expect(openapi.paths).toHaveProperty('/ping');
    expect(openapi.paths).toHaveProperty('/status');
    expect(openapi.components?.schemas).toHaveProperty('Status');
  });
  it('Empty', async () => {
    expect(async () => { await merge([]) }).rejects.toThrow();
    expect(async () => { await merge(['file-1']) }).rejects.toThrow();
  });
});
