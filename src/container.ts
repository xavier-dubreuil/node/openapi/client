import { default as OpenAPIClientAxios, AxiosRequestConfig, OpenAPIClient, OpenAPIV3 } from 'openapi-client-axios';
import { readFile } from 'fs/promises';

export type OpenApiClientDefault = OpenAPIClient<object, object>;

export abstract class ClientService<T extends OpenApiClientDefault = OpenApiClientDefault> {
  constructor (protected readonly client: T) { }
}

export type ClientServices<T extends OpenApiClientDefault> = Record<string, ClientService<T>>
export type ClientServicesConstruct<T extends OpenApiClientDefault> = Record<string, new (client: T) => ClientService<T>>


export abstract class Container<
  O extends OpenApiClientDefault = OpenApiClientDefault,
  S extends ClientServices<O> = ClientServices<O>
> {
  public constructor (
    public readonly client: O,
    public readonly services: S,
    protected readonly axiosRequestConfig: AxiosRequestConfig
  ) { }

  protected static async instantiate<
    NC extends Container<NO, NS>,
    NO extends OpenApiClientDefault = OpenApiClientDefault,
    NS extends ClientServices<NO> = ClientServices<NO>,
  > (
    type: new (client: NO, services: NS, axiosRequestConfig: AxiosRequestConfig) => NC,
    definition: OpenAPIV3.Document,
    axiosRequestConfig: AxiosRequestConfig,
    services?: ClientServicesConstruct<NO> | null
  ): Promise<NC> {
    const api = new OpenAPIClientAxios({ definition, axiosConfigDefaults: axiosRequestConfig });
    const client = await api.init<NO>();
    const servicesList: ClientServices<NO> = {};
    for (const [key, value] of Object.entries(services ?? {})) {
      servicesList[key] = new value(client);
    }

    return new type(client, servicesList as NS, axiosRequestConfig);
  }

  public static async readDefinition (path: string): Promise<OpenAPIV3.Document> {
    return JSON.parse((await readFile(path)).toString());
  }
}
