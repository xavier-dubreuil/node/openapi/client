import { OpenAPIV3 } from 'openapi-types';
import { readFile } from 'fs/promises';

export interface MappingType {
  name?: string
  schema?: OpenAPIV3.SchemaObject
}

export interface EndpointPath {
  endpoint: string
  path: string
}

export type SchemaMapping = Record<string, Record<string, MappingType>>;

const defaultDocument: OpenAPIV3.Document = {
  openapi: '3.0.1',
  info: { title: '', version: '' },
  components: {
    schemas: {},
  },
  paths: {}
}

export class Generator {
  protected openapi: OpenAPIV3.Document;
  protected mapping: SchemaMapping = {};
  protected unmapped: EndpointPath[] = [];

  constructor(openapi: OpenAPIV3.Document | null = null) {
    this.openapi = openapi ?? defaultDocument
  }

  public static async openapiFromFile(path: string): Promise<OpenAPIV3.Document> {
    const buffer = await readFile(path);
    return JSON.parse(buffer.toString()) as OpenAPIV3.Document;
  }

  public setMapping (mapping: SchemaMapping): void {
    this.mapping = mapping;
  }

  public getMapping(endpoint: string, path: string): MappingType | null {
    for (const elem of [endpoint, 'global']) {
      if (this.mapping[elem] === undefined) {
        continue;
      }
      if (this.mapping[elem][path] !== undefined) {
        return this.mapping[elem][path];
      }
    }
    return null;
  }

  public setInfo (info: OpenAPIV3.InfoObject): void {
    this.openapi.info = info;
  }

  public addOperation (path: string, method: OpenAPIV3.HttpMethods, value: OpenAPIV3.OperationObject): void {
    if (this.openapi.paths[path] === undefined) {
      this.openapi.paths[path] = {};
    }
    this.openapi.paths[path]![method] = value;
  }

  public addSchema (name: string, schema: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject): void {
    if (this.openapi.components === undefined) {
      this.openapi.components = {};
    }
    if (this.openapi.components.schemas === undefined) {
      this.openapi.components.schemas = {};
    }
    this.openapi.components.schemas[name] = schema;
  }

  public addComponentSchema (name: string, schema: OpenAPIV3.SchemaObject): OpenAPIV3.ReferenceObject {
    const schemaName = `${name.substring(0, 1).toUpperCase()}${name.substring(1)}`;
    this.addSchema(schemaName, schema);
    return {
      "$ref": `#/components/schemas/${schemaName}`
    };
  }

  public generateSchema (input: string | number | object | string[] | number[] | object[], endpoint: string, path: string[]): OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject {
    const type = typeof input;

    if (Array.isArray(input)) {
      const subTypes = input.map(item => this.generateSchema(item, endpoint, path));
      return {
        type: 'array',
        items: subTypes[0]
      }
    }

    if (type === 'object') {

      const objectPath = `/${path.join('/').replace(/^\//, '')}`;
      const mapping = this.getMapping(endpoint, objectPath);

      if (mapping === null && objectPath.length > 0) {
        this.unmapped.push({endpoint, path: objectPath});
      }

      let schema: OpenAPIV3.SchemaObject;

      if (mapping?.schema !== undefined) {
        schema = mapping.schema;
      } else {
        schema = {
          type: 'object',
          properties: Object.entries(input).reduce((acc, [key, value]) => {
            return { ...acc, [key]: this.generateSchema(value, endpoint, [...path, key]) };
          }, {})
        };
      }

      if (mapping?.name !== undefined) {
        return this.addComponentSchema(mapping.name, schema);
      }
      return schema;
    }

    if (type === 'number') {
      return { type: 'number' };
    }

    return { type: 'string' };
  }

  public merge(openapi: OpenAPIV3.Document): void {
    for (const [name, schema] of Object.entries(openapi.components?.schemas ?? {})) {
      this.addSchema(name, schema);
    }
    for (const [path, methods] of Object.entries(openapi.paths ?? {})) {
      for (const [method, operation] of Object.entries(methods as OpenAPIV3.PathItemObject)) {
        this.addOperation(path, method as OpenAPIV3.HttpMethods, operation as OpenAPIV3.OperationObject);
      }
    }
  }

  public getOpenApi (): OpenAPIV3.Document {
    return this.openapi;
  }

  public toString (): string {
    return JSON.stringify(this.openapi);
  }

  public getUnmapped(): EndpointPath[] {
    return this.unmapped;
  }
}
