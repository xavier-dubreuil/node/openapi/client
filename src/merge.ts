#!/usr/bin/env node

import { argv } from "process";
import { OpenAPIV3 as OpenAPI } from 'openapi-types'
import { Generator } from "./generator";

export async function merge (files: string[]): Promise<OpenAPI.Document> {
  if (files.length < 2) {
    throw new Error('Missing parameters');
  }
  const initial = await Generator.openapiFromFile(files.shift()!);
  const generator = new Generator(initial);

  for (const additional of files) {
    const openapi = await Generator.openapiFromFile(additional);
    generator.merge(openapi);
  }
  return generator.getOpenApi();
}

/* istanbul ignore if */
if (require.main === module) {
  merge(argv.slice(2))
    .then(document => console.log(JSON.stringify(document)))
    .catch(err => console.log(err));
}
